let data = [
  {
    title: "Одежда",
    left: 1,
    right: 22
  },
  {
    title: "Мужская",
    left: 2,
    right: 9
  },
  {
    title: "Женская",
    left: 10,
    right: 21
  },
  {
    title: "Костюмы",
    left: 3,
    right: 8
  },
  {
    title: "Платья",
    left: 11,
    right: 16
  },
  {
    title: "Юбки",
    left: 17,
    right: 18
  },
  {
    title: "Блузы",
    left: 19,
    right: 20
  },
  {
    title: "Брюки",
    left: 4,
    right: 5
  },
  {
    title: "Жакеты",
    left: 6,
    right: 7
  },
  {
    title: "Вечерние",
    left: 12,
    right: 13
  },
  {
    title: "Летние",
    left: 14,
    right: 15
  }
];

(function() {
  let node = document.getElementsByTagName('div');
  drawNestedSetsTree(data, node[0]);

  function drawNestedSetsTree(data, node) {
    if (data.length === 0) {
      return;
    }
    const ul = document.createElement("ul");
    node.appendChild(ul);
    buildTree(data, 1, data.length * 2, ul) // Наибольший правый ключ равен data.length * 2.
  }

  function buildTree(data, start, parentEnd, node) {
    const parent = data.filter(obj => obj.left === start)[0];
    const li = document.createElement("li");
    li.textContent = parent.title;

    node.appendChild(li);

    if (parent.right < (parentEnd - 1)) { // Проверяем есть ли еще элементы этого уровня иерархии.
      const sibling = data.filter(obj => obj.left === (parent.right + 1))[0]; // Если есть берем правый ключ элемента, прибавляем 1 и вызываем рекурсивно функцию.
      buildTree(data, sibling.left, parentEnd, node); // Проходим по всем sibling элементам (прибавлять parent.right + 1), пока правый ключ текущего < (parentEnd - 1).
    }

    const next = data.filter(obj => obj.left === (start + 1))[0]; // Если sibling элемента нет, то берем start + 1 - идем вниз по иерархии, ищем child элементы.

    if (next) {
      const ul = document.createElement("ul"); // Если child элемент есть, создаем список для него
      li.appendChild(ul);
      buildTree(data, next.left, parent.right, ul);
    }
  }
})();